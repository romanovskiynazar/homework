iterable = range(11)


def iter_arr(array):
    for element in array:
        print(element)


iter_arr(iterable)
print()
print()


class Iteration:
    def __init__(self, my_list):
        self.my_list = my_list
        self.length = len(self.my_list) - 1
        self.i = -1

    def __iter__(self):
        return self

    def __next__(self):
        if self.length == self.i:
            raise StopIteration
        self.i += 1
        return self.my_list[self.i]


a = [3, 6, 54, 9, 0]
list_1 = Iteration(a)

for i in list_1:
    print(i)
