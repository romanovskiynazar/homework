def addition(a, b):
    result = a + b
    print(result)


def subtraction(a, b):
    result = a - b
    print(result)


def multiplication(a, b):
    result = a * b
    print(result)


def division(a, b):
    try:
        result = a / b
        print(result)
    except:
        print("Ошибка. На ноль делить нельзя.")
