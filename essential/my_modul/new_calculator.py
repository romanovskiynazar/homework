import Calculator


print("Ноль в качестве знака операции"
        "\nзавершит работу программы")
while True:
    operation = input("Выберите операцию (+, -, *, /): ")
    if operation == "0":
        print("Работа завершена.")
        break
    if operation in ("+", "-", "*", "/"):
        a = float(input("Укажите первое число: "))
        b = float(input("Укажите второе число: "))
        if operation == "+":
            Calculator.addition(a, b)
        elif operation == "-":
            Calculator.subtraction(a, b)
        elif operation == "*":
            Calculator.multiplication(a, b)
        elif operation == "/":
            Calculator.division(a, b)
    else:
        print("Неверный знак операции, попробуйте ещё раз.")

