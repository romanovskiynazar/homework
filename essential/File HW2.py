# Модифицируйте исходный код сервиса по сокращению ссылок из предыдущих двух уроков так, чтобы он
# сохранял базу ссылок на диске и не «забывал» при перезапуске.
import json

link_dict = {
    "Short_link1": "Full_link1",
    "Short_link2": "Full_link2",
    "Short_link3": "Full_link3",
    "Short_link4": "Full_link4",
    "Short_link5": "Full_link5"
}


link_dict.update({"Short_link6": "Full_link6"})

with open("some_dir/HW2.json", "w", encoding="utf-8") as f:
    json.dump(link_dict, f)

with open("some_dir/HW2.json", "r", encoding="utf-8") as f:
    l = json.load(f)
    print(l)


print(link_dict[input("Введите название ссылки: ")])

print(*link_dict, sep=", ")
