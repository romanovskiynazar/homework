# Выведите из списка чисел список квадратов четных чисел. Используйте 2 варианта решения: генератор и
# цикл

my_list = list(range(11))

new_list = [x**2 for x in my_list if x % 2 == 0]
print(new_list)


generator = (x**2 for x in my_list if x % 2 == 0)
print(next(generator))
print(next(generator))
print(next(generator))
print(next(generator))
print(next(generator))
