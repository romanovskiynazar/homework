# Создайте список товаров в интернет-магазине. Сериализуйте его и сохраните в JSON

import json
from random import randint


def create_person():
    person = {
        "name": input("Введите имя: "),
        "lastname": input("Введите фамилию: "),
        "age": randint(10, 50),

    }
    return person


with open("some_dir/myjson.json", "w", encoding="utf-8") as f:
    l = []
    #l.append(json.load(f))
    for i in range(1):
        l.append(create_person())
    json.dump(l, f)

with open("some_dir/myjson.json", "r+", encoding="utf-8") as f:
    l = json.load(f)
    print(l)
    for i in range(1):
        l.append(create_person())
    json.dump(l, f)







