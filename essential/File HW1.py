# Напишите скрипт, который создаёт текстовый файл и записывает в него 10000 случайных действительных
# чисел. Создайте ещё один скрипт, который читает числа из файла и выводит на экран их сумму.

import random

i = 0
my_list = []
for i in range(10000):
    number = random.randint(0, 101)
    my_list.append(str(number))


with open("some_dir/HW1.txt", 'w') as f:
    f.write(", ".join(my_list))


with open("some_dir/HW1.txt", 'r') as f:
    s = f.readline().split(", ")
    lst = []
    for i in s:
        lst.append(int(i))


print(sum(lst))


