# Напишите генератор, который возвращает элементы заданного списка в обратном порядке (аналог
# reversed).

a = [8, 9756, 0, 64, 5]


def generator():
    for i in reversed(a):
        yield i


for i in generator():
    print(i)
