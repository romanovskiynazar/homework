class Worker:
    def __init__(self, name, surname, depart, year):
        self.name = name
        self.surname = surname
        self.depart = depart
        self.year = year

    def __str__(self):
        return f'Имя:{self.name}, Фамилия:{self.surname}, Отдел:{self.depart}, Год поступления:{self.year}'


try:
    a = Worker("Назар", "Григоренко", "разработки", 2001)
    b = Worker("Владимир", "Смыковский", "сборки", 2003)
    c = Worker("митрий", "Гордийчук", "установки", 2005)
    print(a, b)
except TypeError:
    print("Неверный ввод данных")
