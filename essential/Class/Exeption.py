def addition(a, b):
    return f"Результат: {a + b}"


def subtraction(a, b):
    return f"Результат: {a - b}"


def multiplication(a, b):
    return f"Результат: {a * b}"


def division(a, b):
    try:
        return f"Результат: {a / b}"
    except ZeroDivisionError:
        return "На ноль делить нельзя!"


def exponentiations(a, b):
    try:
        return f"Результат: {a ** b}"
    except ZeroDivisionError:
        return "Нельзя возводить ноль в отрицательную степень!"


def operation():
    while True:
        number_1 = (input("a = "))
        number_2 = (input("b = "))
        try:
            number_1 = float(number_1)
            number_2 = float(number_2)
        except ValueError:
            print("Вы должны указать число!")
            continue
        s = input("Укажите тип операции: +, -, *, /, возведение в степень (**): ")
        if s == "+":
            print(addition(number_1, number_2))
        elif s == "-":
            print(subtraction(number_1, number_2))
        elif s == "*":
            print(multiplication(number_1, number_2))
        elif s == "/":
            print(division(number_1, number_2))
        elif s == "**":
            print(exponentiations(number_1, number_2))


operation()
