class Base:
    @staticmethod
    def method():
        print("Hallo from Base")


class Child(Base):
    @staticmethod
    def method():
        print("Hallo from Child")


Base.method()
Child.method()
