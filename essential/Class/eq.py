class A:
    value = 3

    def __eq__(self, other):
        print('A __eq__ called')
        return self.value == other.value


class B(A):
    value = 4

    def __eq__(self, other):
        print('B __eq__ called')
        return self.value == other.value


a, b = A(), B()
print(a == b)
