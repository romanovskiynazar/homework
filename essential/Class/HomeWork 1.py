class Review:
    # text
    # rating
    def __init__(self, rating, text=""):
        self.rating = rating
        self.text = text

    def __repr__(self):
        return f"Rating: {self.rating},\n Text: {self.text}\n"


class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre
        self.reviews = []
        for i in range(10):
            self.reviews.append(Review(i))

    def __str__(self):
        return f"Автор:{self.author}, Название книги:{self.name}, Год:{self.year}, Жанр:{self.genre}\n {self.reviews}"

    def __repr__(self):
        return f"Автор:{self.author}, Название книги:{self.name}, Год:{self.year}, Жанр:{self.genre}"

    def __eq__(self, other):
        return self.author == other.author and self.name == other.name and \
               self.year == other.year and self.genre == other.genre


book_a = Book("Дэн Браун", "Код да Винчи", 2003, "Детектив")
book_b = Book("Франк Тилье", "Головоломка", 2013, "Психологочиский триллер")
book_c = Book("Мариам Петросян", "Дом, в котором...", 2009, "Фентези")
book_d = Book("Дэн Браун", "Код да Винчи", 2003, "Детектив")
book_e = Book("Дэн Браун", "Код да Винчи", 2005, "Детектив")

print(book_a, book_b, book_c)
print(book_a == book_b)
print(book_a == book_d)
print(book_a == book_e)





