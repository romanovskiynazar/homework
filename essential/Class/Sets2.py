# Создайте программу, которая эмулирует работу сервиса по сокращению ссылок. Должна быть
# реализована возможность ввода изначальной ссылки и короткого названия и получения изначальной
# ссылки по её названию.

link_dict = {
    "Short_link1": "Full_link1",
    "Short_link2": "Full_link2",
    "Short_link3": "Full_link3",
    "Short_link4": "Full_link4",
    "Short_link5": "Full_link5"
}


link_dict.update({"Short_link6": "Full_link6"})

print(link_dict[input("Введите название ссылки: ")])


print(*link_dict, sep=", ")

