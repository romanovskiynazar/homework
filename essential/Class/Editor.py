class Editor:
    def view_document(self):
        print("Документ")

    def edit_document(self):
        print("Редактирование документов недоступно для бесплатной версии.")


class ProEditor(Editor):
    def edit_document(self):
        print("Редактирование документов доступно")


password = int(input("Введите пароль: "))
if password == 777:
    e = ProEditor()
else:
    e = Editor()

e.edit_document()
e.view_document()
