class Iteration:
    def __init__(self, my_list):
        self.my_list = my_list
        self.length = len(self.my_list)
        self.value = 0

    def __iter__(self):
        return self

    def __next__(self):
        if not self.length + self.value:
            raise StopIteration
        self.value -= 1
        return self.my_list[self.value]


a = [3, 6, 54, 9, 0]
list_1 = Iteration(a)

for i in list_1:
    print(i)
