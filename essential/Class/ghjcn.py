class Temp:
    """
    @self.cvalue
    @self.fvalue
    """

    def __init__(self, val, unit='C'):
        self.set_temp(val, unit)

    def get_temp(self, unit='C'):
        if unit == 'C':
            return self.cvalue
        elif unit == 'F':
            return self.fvalue

    def set_temp(self, val, unit='C'):
        if unit == 'C':
            self.cvalue = val
            self.fvalue = val * 9 / 5 + 32
        elif unit == 'F':
            self.cvalue = (val - 32) * 5 / 9
            self.fvalue = val


a = Temp(17)

print(a.get_temp())
a.set_temp(8)
print(a.get_temp('F'))
