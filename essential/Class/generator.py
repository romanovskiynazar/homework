# Напишите функцию-генератор для получения n первых простых чисел.

def prime_number(n):
    for i in range(2, n + 1):
        number = True
        for j in range(2, i):
            if i % j == 0:
                number = False
        if number:
            yield i


for i in prime_number(19):
    print(i)
