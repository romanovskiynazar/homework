class Employer:
    a = "Employer"


class Developer(Employer):
    pass


class Manager:
    a = "Manager"


class TL(Developer, Manager):
    pass


e1 = TL()
print(e1.a)
