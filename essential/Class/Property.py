class Temp:
    """
    @self.cvalue
    @self.fvalue
    """

    def __init__(self, val, unit='C'):
        self.temperature(val, unit)

    @property
    def temperature(self, unit='C'):
        if unit == 'C':
            return self.cvalue
        elif unit == 'F':
            return self.fvalue

    @temperature.setter
    def temperature(self, val, unit='C'):
        if unit == 'C':
            self.cvalue = val
            self.fvalue = val * 9 / 5 + 32
        elif unit == 'F':
            self.cvalue = (val - 32) * 5 / 9
            self.fvalue = val


a = Temp(17)

print(a.temperature())
a.temperature(8)
# print(a.temperature('F'))
