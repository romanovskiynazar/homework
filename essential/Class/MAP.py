def my_map_comprehension(func, *seq):
    return [func(*args) for args in zip(*seq)]
    #for args in zip(*seq):
        #res.append(func(*args))
    #return res


def my_map_f_generator(func, *seq):
    for args in zip(*seq):
        yield args


def my_map(func, *seq):
    return (func(*args) for args in zip(*seq))


print(my_map_comprehension(abs, [-1, 0, 1]))


for _ in map(print, my_map(abs, [-1,  0, 1])):
    pass
