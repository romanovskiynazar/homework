# Создайте список целых чисел. Получите список квадратов нечётных чисел из этого списка.

my_list = [1, 6, 3, 2, 7, 9, 3, 0, 5]


def foo(numbers):
    neg_numbers = filter(lambda x: x % 2 != 0, numbers)
    new_list = []
    for i in neg_numbers:
        a = i ** 2
        new_list.append(a)
    print(new_list)


foo(my_list)


