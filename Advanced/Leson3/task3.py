# Создайте функцию-генератор чисел Фибоначчи. Примените к ней декоратор, который будет оставлять в
# последовательности только чётные числа.

def decorator(fn):

    def decorated_fn(*args):
        return filter(lambda x: x % 2 == 0, fn(*args))

    return decorated_fn


@decorator
def fibonacci_generator(count):
    value_1, value_2 = 0, 1

    for _ in range(count):
        value_1, value_2 = value_2, value_1 + value_2
        yield value_1


fibonacci_iterator = fibonacci_generator(10)

for number in fibonacci_iterator:
    print(number)
