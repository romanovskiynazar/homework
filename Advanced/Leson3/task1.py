# Создайте обычную функцию умножения двух чисел. Частично примените её к одному аргументу.
# Создайте каррированную функцию умножения двух чисел. Частично примените её к одному аргументу.

from functools import partial


def ordinary_mul(x, y):
    return x * y


part_mul = partial(ordinary_mul, 5)
print(part_mul(2))
print(part_mul(3))


def curryied_mul(x):
    def do_mul(y):
        return x * y

    return do_mul


add_to_five = curryied_mul(5)
print(add_to_five(2))
print(add_to_five(3))
