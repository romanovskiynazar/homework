import asyncio

import aiohttp
import requests
import time

urls = [
    "http://youtube.com",
    "http://twitch.tv",
    "http://github.com",
    "http://gitlab.com",
    "http://instagram.com",
    "http://amazon.com",
    "http://facebook.com",
    "http://apple.com",
    "http://stackoverflow.com",
    "http://wikipedia.org",
    "http://yandex.ua",
]


def sync(urls):
    start_time = time.time()

    for url in urls:
        start_request = time.time()
        _ = requests.get(url)
        print(url, f"time: {time.time() - start_request}")

    print(time.time() - start_time)


async def req(url, session):
    start_request = time.time()
    async with session.get(url) as response:
        r = await response.read()
        print(url, f"time: {time.time() - start_request}")
        return r


async def asynchronus(urls):
    start_time = time.time()
    async with aiohttp.ClientSession() as session:
        tasks = []
        for url in urls:
            tasks.append(req(url, session))
        _ = await asyncio.gather(*tasks)

    print(time.time() - start_time)


asyncio.get_event_loop().run_until_complete(asynchronus(urls))
sync(urls)
