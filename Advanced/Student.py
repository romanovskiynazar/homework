import json

STORAGE_PATH = 'students.json'


def in_student():
    name = input('Enter name: ')
    lastname = input('Enter lastname: ')
    return {
        "Name": name,
        "Lastname": lastname
    }


def students_store(data):
    with open(STORAGE_PATH, mode='w') as storage:
        json.dump(data, storage, indent=4)


def get_students():
    try:
        with open(STORAGE_PATH, mode='r') as storage:
            return json.load(storage)
    except FileNotFoundError:
        return {"Person": []}


def main():
    data = get_students()
    # print("Студенты на курсе: ", *data)

    while True:
        choice = input("Enter (1) for add new student or (2) for get list all students: ")
        if choice == "1":
            try:
                data["Person"].append(in_student())
            finally:
                students_store(data)
        elif choice == "2":
            print(get_students())


if __name__ == '__main__':
    main()
