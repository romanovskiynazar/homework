# Задание 1
# Пройтись по диапазону чисел от 0 до 100, выводить все числа, при этом пропустить число 4 и,
# как только цикл достигнет числа 18 - выйти из цикла.

# Цикл while
x = 0
while x < 101:
    x += 1

    if x == 4:
        continue
    if x == 19:
        break
    print(x)
print(end="\n\n")


# Цикл while
for x in range(0, 101):
    x += 1

    if x == 4:
        continue
    if x == 19:
        break
    print(x)
