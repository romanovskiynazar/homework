# Задание 2
# В предложении “Hello world” заменить все буквы “o” на “a” , а буквы “l”  на “e”.

STRING = "Hello world!"

# цикл со счетчиком while:
result = ""
i = 0

while i < len(STRING):
    if STRING[i] == "o":
        result += "a"
    elif STRING[i] == "l":
        result += "e"
    else:
        result += STRING[i]
    i += 1

print(result, end='\n\n')


# цикл со счетчиком for:
result = ""

for i in range(len(STRING)):
    if STRING[i] == "o":
        result += "a"
    elif STRING[i] == "l":
        result += "e"
    else:
        result += STRING[i]
    i += 1

print(result)
