# Задание 3
# Напишите программу-калькулятор, в которой пользователь сможет ввести тип операции,
# ввести необходимые числа и получить результат. Операции, которые необходимо реализовать:
# сложение,  вычитание,  умножение,  деление,  возведение  в  степень,  синус,  косинус  и  тангенс
# числа.

import math

s = input("""Укажите тип операции: +, -, *, /, возведение в степень (**), sin, cos, tan
: """)

a = float(input("a = "))

if s == "+":
    b = float(input("b = "))
    result = a + b
elif s == "-":
    b = float(input("b = "))
    result = a - b
elif s == "*":
    b = float(input("b = "))
    result = a * b
elif s == "/":
    b = float(input("b = "))
    result = a / b
elif s == "**":
    b = float(input("b = "))
    result = a ** b
elif s == "sin":
    result = math.sin(a)
elif s == "cos":
    result = math.cos(a)
elif s == "tan":
    result = math.tan(a)
else:
    print("Неверная операция")

print(result)
