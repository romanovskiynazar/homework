# Задание 3
# Напишите программу-калькулятор, в которой пользователь сможет ввести тип операции,
# ввести необходимые числа и получить результат. Операции, которые необходимо реализовать:
# сложение,  вычитание,  умножение,  деление,  возведение  в  степень,  синус,  косинус  и  тангенс
# числа.

import math

print("Ноль в качестве знака операции завершит работу программы")
s = input("""Укажите тип операции: 
сложение, 
вычитание, 
умножение, 
деление,
возведение в степень, 
синус, 
косинус 
тангенс
: """)

if s == "сложение":
    a = float(input("a = "))
    b = float(input("b = "))
    print(a+b)
elif s == "вычитание":
    a = float(input("a = "))
    b = float(input("b = "))
    print(a-b)
elif s == "умножение":
    a = float(input("a = "))
    b = float(input("b = "))
    print(a*b)
elif s == "деление":
    a = float(input("a = "))
    b = float(input("b = "))
    print(a/b)
elif s == "возведение в степень":
    a = float(input("a = "))
    b = float(input("b = "))
    print(a**b)
elif s == "синус":
    a = float(input("a = "))
    print(math.sin(a))
elif s == "косинус":
    a = float(input("a = "))
    print(math.cos(a))
elif s == "тангенс":
    a = float(input("a = "))
    print(math.tan(a))
elif s:
    print("Завершение программы")
else:
    print("Неверная операция")
