# Задание 3
# Создайте  программу-калькулятор,  которая  поддерживает  четыре  операции:  сложение,
# вычитание, умножение, деление. Все данные должны вводиться в цикле, пока пользователь не
# укажет,  что  хочет  завершить  выполнение  программы.  Каждая  операция  должна  быть
# реализована  в  виде  отдельной  функции.  Функция  деления  должна  проверять  данные  на
# корректность и выдавать сообщение об ошибке в случае попытки деления на ноль.

def addition(a, b):
    result = a + b
    print(result)


def subtraction(a, b):
    result = a - b
    print(result)


def multiplication(a, b):
    result = a * b
    print(result)


def division(a, b):
    try:
        result = a / b
        print(result)
    except:
        print("Ошибка. На ноль делить нельзя.")


def calculator():
    print("Ноль в качестве знака операции"
          "\nзавершит работу программы")
    while True:
        operation = input("Выберите операцию (+, -, *, /): ")
        if operation == "0":
            break
        if operation in ("+", "-", "*", "/"):
            a = float(input("Укажите первое число: "))
            b = float(input("Укажите второе число: "))
            if operation == "+":
                return addition(a, b)
            elif operation == "-":
                return subtraction(a, b)
            elif operation == "*":
                return multiplication(a, b)
            elif operation == "/":
                return division(a, b)
        else:
            print("Неверный знак операции, попробуйте ещё раз.")


def main():
    calculator()


if __name__ == '__main__':
    main()
