# Задание 3
# Простым называется число, которое делится нацело только на единицу и само себя. Число 1 не
# считается простым. Напишите программу, которая находит все простые числа в заданном
# промежутке, выводит их на экран, а затем по требованию пользователя выводит их сумму либо
# произведение.

import math

new_list = []
my_list = list(range(2, 20))


def prime():
    for list_element in my_list:
        prime = True
        for i in range(2, list_element):
            if list_element % i == 0:
                prime = False
        if prime:
            new_list.append(list_element)
    return new_list


#print(sum(new_list))
#print(math.prod(new_list))
