# Задание 1
# Создайте список и введите его значения. Найдите наибольший и наименьший элемент списка, а
# также сумму и среднее арифметическое его значений.

list = [1, 5, -7, 4, 9, -5]


def max_value():
    max_element = list[0]
    for element in list:
        if element > max_element:
            max_element = element
    print("Наибольший элемент списка: ", max_element)


def min_value():
    min_element = list[0]
    for element in list:
        if element < min_element:
            min_element = element
    print("Наименьший элемент списка: ", min_element)


def sum_list():
    print("Сумма всех элементов списка: ", sum(list))


def arithmetic_mean():
    a = sum(list)
    b = a / len(list)
    print("Среднее арифметическое значение всех елементов списка: ", round(b, 2))


max_value()
min_value()
sum_list()
arithmetic_mean()
