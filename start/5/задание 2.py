# Задание 2
# Создайте программу, которая проверяет, является ли палиндромом введённая фраза.

def palindrome():
    str = input("Введите фразу: ")
    l = len(str)
    p = l-1
    index = 0
    while index < p:
        if str[index] == str[p]:
            index = index + 1
            p = p-1
            print("Введённая фраза является палиндромом.")
            break
        else:
            print("Введённая фраза не является палиндромом")
            break


palindrome()
